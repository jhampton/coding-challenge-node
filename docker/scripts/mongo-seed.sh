#!/bin/bash

if [ -e /data/.seed-data ]
then
  printf "\n========== Code Challenge database already seeded! ========== \n"
  printf "run 'rm docker/data/.seed-data' to drop and reseed\n\n"
else
    printf "\n========== REMOVING Code Challenge Database ========== \n"
    mongo code-challenge --host code-challenge-mongodb --eval "db.dropDatabase()"

    printf "\n========== Importing documents into new Code Challenge Database ========== \n"
    printf "=============================================================================\n"
    mongoimport --host code-challenge-mongodb --db code-challenge --collection employee --file /data/employees.json --jsonArray
    mongoimport --host code-challenge-mongodb --db code-challenge --collection user --file /data/users.json --jsonArray

  touch /data/.seed-data
  printf "\nDONE!\n"
fi
