const express = require('express');
const { login } = require('./user');

const route = 'user';
const router = express.Router();

router.post(`/${route}/login`, login);

module.exports = router;
