const debug = require('debug')('code-challenge:user');
const { confirmPassword } = require('../../lib/bcrypt');
const { signToken } = require('../../lib/jwt');
const User = require('../../models/User');

/**
 * Validates an email and password
 * @param  {object} req - Express request object
 * @param  {object} res - Express response object
 * @return {object} - Signed User
 */
async function login(req, res) {
  try {
    const { email, password } = req.body;

    const user = await User.findByEmail(email);
    if (!user) throw new Error('Login failed - please verify email and password');

    const validPassword = await confirmPassword(password, user.password);
    if (!validPassword) throw new Error('Invalid username or password provided');

    // creates a token for this user, excludes some fields
    const signedUser = await signToken(user, ['password']);

    return res.json(signedUser);
  } catch (err) {
    debug('%O', err);
    return res.status(400).json({ message: 'Invalid username or password provided' });
  }
}

module.exports = {
  login,
};
