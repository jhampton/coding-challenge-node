const debug = require('debug')('code-challenge:employee');
const Employee = require('../../models/Employee');

/**
 * Return an employee by Id
 * @param  {object} req - Express request object
 * @param  {object} res - Express response object
 * @return {array} - Employees
 */
async function findById(req, res) {
  try {
    const { id } = req.params;

    const employee = await Employee.findById(id);
    return res.json(employee);
  } catch (error) {
    debug('%O', error);
    return res.status(400).json({ message: 'Unable to locate employee' });
  }
}

/**
 * Return all employees
 * @param  {object} req - Express request object
 * @param  {object} res - Express response object
 * @return {array} - Employees
 */
async function findAll(req, res) {
  try {
    const employees = await Employee.findAll();
    return res.json(employees);
  } catch (error) {
    debug('%O', error);
    return res.status(400).json({ message: 'Unable to find all employees' });
  }
}

/**
 * Add an employee
 * @param  {object} req - Express request object
 * @param  {object} res - Express response object
 * @return {object} - Created employee
 */
async function add(req, res) {
  try {
    const { body: employee } = req;

    const addedEmployee = await Employee.insert(employee);
    return res.json(addedEmployee);
  } catch (err) {
    debug('%O', err);
    return res.status(400).json({ message: 'Unable to add employee' });
  }
}

/**
 * Update an employee
 * @param  {object} req - Express request object
 * @param  {object} res - Express response object
 * @returns {object} - Updated employee
 */
async function update(req, res) {
  try {
    const { id } = req.params;
    const { body: employee } = req;

    await Employee.update(id, employee);
    const updatedEmployee = await Employee.findById(id);
    return res.json(updatedEmployee);
  } catch (error) {
    debug('%O', error);
    return res.status(400).json({ message: 'Unable to update employee' });
  }
}

/**
 * Delete an employee
 * @param  {object} req - Express request object
 * @param  {object} res - Express response object
 * @returns {object} - Deleted employee id
 */
async function remove(req, res) {
  try {
    const { id } = req.params;

    await Employee.delete(id);
    return res.json({ id });
  } catch (error) {
    debug('%O', error);
    return res.status(400).json({ message: 'Unable to delete employee' });
  }
}

module.exports = {
  findById,
  findAll,
  add,
  update,
  remove,
};
