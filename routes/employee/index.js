const express = require('express');
const { isAuthenticated } = require('../../middleware/authenticate');
const { findById, findAll, add, update, remove } = require('./employee');

const route = 'employee';
const router = express.Router();

router.all(`/${route}`, isAuthenticated);
router.all(`/${route}/*`, isAuthenticated);

router.get(`/${route}/:id`, findById);
router.get(`/${route}`, findAll);
router.post(`/${route}`, add);
router.put(`/${route}/:id`, update);
router.delete(`/${route}/:id`, remove);

module.exports = router;
