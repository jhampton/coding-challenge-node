# coding-challenge-node

This app consists of a REST API that includes the below listed functionality. Data is persisted in MongoDB.

* Get employees by an ID
* Create new employee
* Update existing employee
* Delete employee
* Get all employees

## Prerequisites

This app is composed of multiple containers and is managed using docker compose.

To get started, you will need to [install Docker](https://docs.docker.com/install/)

## Getting Started - API Documentation

Available routes and example usage can be found in the [API documentation](https://documenter.getpostman.com/view/200008/RWMCtUqb)

## Local Development

* If you receive a permission denied error when running docker-compose up, run the following command `chmod +x docker/scripts/mongo-seed.sh`

#### Build containers, seed the database, and start the app

`$ docker-compose up --build`

#### Start the app

`$ docker-compose up`

#### Start the app in detached mode

`$ docker-compose up -d`

#### Stop the app (running containers)

`$ docker-compose down`

#### Force database reseed

`$ rm docker/data/.seed-data`

#### Debugging

To debug the application, open [chrome inspect](chrome://inspect/#devices) and either select the **Open dedicated DevTools for Node** link or the **Remote Target** for **file:///usr/src/app/index.js**

#### Clean up dangling images

`$ docker rmi $(docker images -f "dangling=true" -q)`

#### Prune volumes

`$ docker volume prune`

## Directory Structure

* api                 - API process, initializes REST routes
* config              - API configuration
* docker              - Dockerfiles, mongo seed data, and scripts used by the docker process
* lib                 - Modules that contain shared logic
* middleware          - API middleware modules (authenticate, etc.)
* models              - Houses domain logic for entities
* repo                - Data persistence and retrieval for models
* routes              - REST routes
* server              - Modules used to boot the API
* service-providers   - Modules that abstract a service like Mongo or Rabbit
