const debug = require('debug')('code-challenge');
const jwt = require('jsonwebtoken');
const { jwtSecret } = require('../config');

/**
 * Ensures an Authorization header contains a valid JWT
 * @param {object} req - Express request object
 * @param {object} res - Express response object
 * @param {function} next - Next middleware to invoke
 * @return {*} - Response from next middleware
 */
function isAuthenticated(req, res, next) {
  if (!req.headers.authorization) {
    debug(`Auth Error=No authorization header, url=${req.url}`);
    return res.status(403).json({ error: 'You are not permitted to perform this action' });
  }

  const token = req.headers.authorization.replace('Bearer ', '');

  jwt.verify(token, jwtSecret, (err, decoded) => {
    if (err) {
      debug(`Auth Error=Unable to verify jwt, url=${req.url}, token=${token}, err=${err}`);
      return res.status(403).json({ error: 'You are not permitted to perform this action' });
    }

    // account for documents with _id
    if (!decoded.id && !decoded['_id']) {
      debug(`Auth Error=Invalid decoded jwt, decoded=${decoded}`);
      return res.status(403).json({ error: 'You are not permitted to perform this action' });
    }

    // add decoded jwt to request
    req.currentUser = decoded;

    return next();
  });
}

module.exports = {
  isAuthenticated,
};
