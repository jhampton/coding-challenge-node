const UserRepo = require('../repo/UserRepo');

const User = {
  async findByEmail(email) {
    const user = await UserRepo.findByEmail(email);
    return this.fromBson(user);
  },

  // preps an object for the UI
  fromBson(employee) {
    return employee;
  },

  // preps an object for storage
  toBson(employee) {
    return employee;
  },
};

module.exports = User;
