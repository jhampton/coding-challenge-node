const clone = require('lodash/clone');
const EmployeeRepo = require('../repo/EmployeeRepo');

const Employee = {
  async findById(id) {
    const employee = await EmployeeRepo.findById(id);
    return this.fromBson(employee);
  },

  async findAll() {
    const employees = await EmployeeRepo.findAll();
    return employees.map(e => this.fromBson(e));
  },

  async insert(employee) {
    const employeeToInsert = this.toBson(employee);
    return EmployeeRepo.insert(employeeToInsert);
  },

  async update(id, employee) {
    const employeeToUpdate = this.toBson(employee);
    return EmployeeRepo.update(id, employeeToUpdate);
  },

  async delete(id) {
    return EmployeeRepo.delete(id);
  },

  // preps an object for the UI
  fromBson(employee) {
    return employee;
  },

  // preps an object for storage
  toBson(employee) {
    const cloned = clone(employee);
    delete cloned['_id'];
    return cloned;
  },
};

module.exports = Employee;
