const mongo = require('../service-providers/mongo');

const UserRepo = {
  collection: 'user',

  findByEmail(email) {
    return mongo.findOne(this.collection, { email });
  },
};

module.exports = UserRepo;
