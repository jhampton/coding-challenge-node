const ObjectId = require('mongodb').ObjectID;
const mongo = require('../service-providers/mongo');

const status = {
  ACTIVE: 'ACTIVE',
  INACTIVE: 'INACTIVE',
};

const EmployeeRepo = {
  collection: 'employee',

  findById(id) {
    return mongo.findOne(this.collection, { _id: ObjectId(id), status: status.ACTIVE });
  },

  findAll() {
    return mongo.findAll(this.collection, { status: status.ACTIVE });
  },

  insert(employee) {
    return mongo.insertOne(this.collection, employee);
  },

  update(id, employee) {
    return mongo.updateOne(this.collection, { _id: ObjectId(id) }, { $set: employee });
  },

  delete(id) {
    return mongo.updateOne(this.collection, { _id: ObjectId(id) }, { $set: { status: status.INACTIVE } });
  },
};

module.exports = EmployeeRepo;
