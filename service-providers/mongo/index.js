const { MongoClient } = require('mongodb');
const debug = require('debug')('code-challenge');
const { databaseName, mongoDbUri } = require('../../config');

let client = null;
let db = null;

/**
 * If not already connected, connects to a mongo database
 * @return {void}
 */
async function mongoConnect() {
  if (!client || !client.isConnected() || !db) {
    client = await MongoClient.connect(
      mongoDbUri,
      { useNewUrlParser: true },
    );
    db = client.db(databaseName);
    debug(`${process.pid} connected to mongo`);
  }
}

/**
 * Find all documents for a given collection
 * @param {string} collection - Name of the collection
 * @param {object} query - Query to execute against collection
 * @param {object} options - Options to apply against query
 * @return {object} - result from mongo
 */
async function findAll(collection, query = {}, options = {}) {
  await mongoConnect();
  return db
    .collection(collection)
    .find(query, options)
    .toArray();
}

/**
 * Find one document
 * @param {string} collection - Name of the collection
 * @param {object} query - Query to execute against collection
 * @return {object} - result from mongo
 */
async function findOne(collection, query) {
  await mongoConnect();
  return db.collection(collection).findOne(query);
}

/**
 * Insert one document
 * @param {string} collection - Name of the collection
 * @param {object} document - Document object to insert
 * @return {object} - result from mongo
 */
async function insertOne(collection, document) {
  await mongoConnect();
  const { ops } = await db.collection(collection).insertOne(document);
  return ops[0];
}

/**
 * Updates one document
 * @param {string} collection - Name of the collection
 * @param {object} query - Query to execute against collection
 * @param {object} set - Fields to Update
 * @return {object} - result from mongo
 */
async function updateOne(collection, query, set) {
  await mongoConnect();
  const { result } = await db.collection(collection).updateOne(query, set);
  return result;
}

module.exports = {
  mongoConnect,
  findAll,
  findOne,
  insertOne,
  updateOne,
};
