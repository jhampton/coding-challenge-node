const cluster = require('cluster');
const numCPUs = require('os').cpus().length;
const debug = require('debug')('code-challenge');
const { enableCluster, workerCount } = require('../config');

/**
 * Boot clustered process
 * @returns {void}
 */
function bootCluster() {
  if (cluster.isMaster) {
    debug(`Master ${process.pid} is running`);
    const workers = workerCount || numCPUs;

    // spin up a worker per cpu core or provided worker count
    for (let i = 0; i < workers; i += 1) {
      cluster.fork();
    }

    // spin up a new worker when another exits (crashes)
    cluster.on('exit', (worker, code, signal) => {
      debug(`Worker ${worker.process.pid} died with code ${code} and signal ${signal}`);
      cluster.fork();
    });
  } else {
    debug(`Worker ${process.pid} is running`);
  }
}

/**
 * Handle any boot up logic, boot the server
 * @returns {void}
 */
function bootServer() {
  // boot clustered server
  if (enableCluster) bootCluster();
  // boot non-clustered server
  if (!enableCluster) require('./api')();
}

module.exports = {
  bootServer,
};
