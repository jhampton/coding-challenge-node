const http = require('http');
const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');
const logger = require('morgan');
const debug = require('debug')('code-challenge');
const { initializeRoutes } = require('../api/');
const { mongoConnect } = require('../service-providers/mongo');

/**
 * Initialize the express api
 * @returns {void}
 */
function initializeApi() {
  const app = express();
  const port = process.env.PORT || 3004;
  const server = http.createServer(app);

  app.set('port', port);
  app.use(logger('dev'));
  app.use(cors());
  app.use(bodyParser.json());
  app.use(bodyParser.urlencoded({ extended: false }));

  /**
   * On Error callback
   * @param {object} error - Error object
   * @returns {void}
   */
  function onError(error) {
    debug('%O', error);
  }

  /**
   * On listening callback
   * @returns {void}
   */
  function onListening() {
    debug(`${process.pid} listening on port ${port}`);
  }

  server.listen(port);
  server.on('error', onError);
  server.on('listening', onListening);

  // connect to persistence, initialize routes
  mongoConnect();
  initializeRoutes(app);

  // catch all error handler
  // this must be defined after other app.use() and routes calls
  app.use((req, res, next) => {
    const route = (req && req.originalUrl) || 'no original route';
    debug(`api error: ${route}`);
    next();
  });

  // 404 handler - must be at very end of stack as we don't invoke next()
  app.use((req, res) => {
    res.status(404).json({ message: 'The requested resource could not be found' });
  });
}

module.exports = initializeApi;
