const bcrypt = require('bcrypt');
const { saltRounds } = require('../config');

/**
 * Confirms a user password
 * @param {string} password - Incoming clear text password
 * @param {string} hashedPassword - Hashed password from User document
 * @returns {boolean} - true if password matches
 */
async function confirmPassword(password, hashedPassword) {
  try {
    return bcrypt.compare(password, hashedPassword);
  } catch (err) {
    throw new Error('Could not validate user credentials');
  }
}

/**
 * Return a hashed password
 * @param {string} password - Password to hash
 * @returns {string} - hashed password
 */
function hashPassword(password) {
  return bcrypt.hash(password, saltRounds).then(hash => hash);
}

module.exports = {
  confirmPassword,
  hashPassword,
};
