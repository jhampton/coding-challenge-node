const jwt = require('jsonwebtoken');
const { jwtSecret, tokenExpires } = require('../config');

/**
 * Returns an encrypted token created from the jwt payload and secret key
 * @param {object} jwtPayload - Object used to create the jwt
 * @param {array} keysToDelete - Array of keys to delete from jwt payload prior to signing
 * @returns {object} - payload with signed token
 */
function signToken(jwtPayload, keysToDelete) {
  const cleanedPayload = Object.assign({}, jwtPayload);
  keysToDelete.forEach(k => delete cleanedPayload[k]);

  return new Promise((resolve, reject) => {
    jwt.sign(cleanedPayload, jwtSecret, { expiresIn: tokenExpires }, (err, token) => {
      if (err) reject(err);
      return resolve(Object.assign(cleanedPayload, { access_token: token, status: 'authenticated' }));
    });
  });
}

module.exports = {
  signToken,
};
