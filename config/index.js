const joi = require('joi');

const defaultMongo = 'mongodb://localhost:27017';
const defaultDbName = 'code-challenge';
const defaultCluster = false;
const defaultSaltRounds = 10;
const defaultTokenExpires = '7d';

const envVarsSchema = joi
  .object({
    NODE_ENV: joi
      .string()
      .valid(['local', 'development', 'production'])
      .required(),
    ENABLE_CLUSTER: joi
      .boolean()
      .default(defaultCluster),
    MONGODB_URI: joi.string().default(defaultMongo),
    JWT_SECRET: joi.string().required(),
    DATABASE_NAME: joi.string().default(defaultDbName),
    WORKER_COUNT: joi.number().max(10),
    SALT_ROUNDS: joi.number().default(defaultSaltRounds),
    TOKEN_EXPIRES: joi.string().default(defaultTokenExpires),
  })
  .unknown() // allow variables not defined here
  .required(); // require the tested object, in this case process.env

// validate process.env
const { error, value: envVars } = joi.validate(process.env, envVarsSchema);
if (error) throw new Error(`Config validation error: ${error.message}`);

const config = {
  env: envVars.NODE_ENV,
  enableCluster: envVars.ENABLE_CLUSTER,
  mongoDbUri: envVars.MONGODB_URI,
  jwtSecret: envVars.JWT_SECRET,
  databaseName: envVars.DATABASE_NAME,
  workerCount: envVars.WORKER_COUNT,
  saltRounds: envVars.SALT_ROUNDS,
  tokenExpires: envVars.TOKEN_EXPIRES,
};

module.exports = config;
