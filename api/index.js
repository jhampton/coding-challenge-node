const employee = require('../routes/employee');
const user = require('../routes/user');

/**
 * Initialize routes
 * @param {object} app - Express app object
 * @returns {void}
 */
function initializeRoutes(app) {
  app.use('/', employee);
  app.use('/', user);
}

module.exports = {
  initializeRoutes,
};
