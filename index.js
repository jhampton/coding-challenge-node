/*
 * App entry point
 */
const cluster = require('cluster');
const { enableCluster } = require('./config');
const { bootServer } = require('./server');

/**
 * Start a non-clustered process
 * @return {void}
 */
function startWorkerProcess() {
  if (enableCluster && cluster.isWorker) {
    require('./server/api')();
  }
}

bootServer();
startWorkerProcess();
